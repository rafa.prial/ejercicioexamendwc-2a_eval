// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyCLzIiCNGtPbHPCGp6S6_FS1aqCVQbevh8",

    authDomain: "libreria-angular.firebaseapp.com",

    projectId: "libreria-angular",

    storageBucket: "libreria-angular.appspot.com",

    messagingSenderId: "390385446564",

    appId: "1:390385446564:web:2c8e22fb9b9bd62bb84031"

  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
