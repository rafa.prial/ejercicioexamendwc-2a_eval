import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LibroService } from './../../../services/libros/libro.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-libros',
  templateUrl: './list-libros.component.html',
  styleUrls: ['./list-libros.component.css'],
})
export class ListLibrosComponent implements OnInit {
  libros: any[] = [];
  constructor(
    private _libroService: LibroService,
    private toastr: ToastrService,
  ) {}

  ngOnInit(): void {
    this.getLibros();

  }
  getLibros() {
    this._libroService.getLibros().subscribe((data) => {
      this.libros = [];
      data.forEach((element: any) => {
        this.libros.push({
          id: element.payload.doc.id,
          ...element.payload.doc.data(),
        });
      });
    });
  }
  eliminarLibro(id: string) {
    this._libroService
      .eliminarLibro(id)
      .then(() => {
        this.toastr.error('Libro eliminado con exito', 'Libro Eliminado');
      })
      .catch((error) => {
        console.log(error);
      });
  }
}
