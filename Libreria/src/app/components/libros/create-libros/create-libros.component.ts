import { LibroService } from './../../../services/libros/libro.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-create-libros',
  templateUrl: './create-libros.component.html',
  styleUrls: ['./create-libros.component.css'],
})
export class CreateLibrosComponent implements OnInit {
  createLibro: FormGroup;
  submitted = false;
  loading = false;
  id: string | null;
  titulo = 'Agregar Libro';
  constructor(
    private fb: FormBuilder,
    private _libroService: LibroService,
    private router: Router,
    private toastr: ToastrService,
    private aRoute: ActivatedRoute
  ) {
    this.createLibro = this.fb.group({
      nombre: ['', Validators.required],
      autor: ['', Validators.required],
      descripcion: ['', Validators.required],
      precio: ['', Validators.required],
      categoria: ['', Validators.required],
      portada: [''],
    });
    this.id = this.aRoute.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.edLibro();
  }

  agregarEditarLibro() {
    this.submitted = true;
    if (this.createLibro.invalid) {
      return;
    }
    if (this.id == null) {
      this.agregarLibro();
    } else {
      this.editarLibro(this.id);
    }
  }

  agregarLibro() {
    const libro: any = {
      nombre: this.createLibro.value.nombre,
      autor: this.createLibro.value.autor,
      descripcion: this.createLibro.value.descripcion,
      precio: this.createLibro.value.precio,
      categoria: this.createLibro.value.categoria,
      portada: this.createLibro.value.portada,
      fechaActualizacion: new Date(),
      fechaCreacion: new Date(),
      prestado: false,
      usuario: '',
      fechaDevolucion: new Date(),
    };
    if (libro.portada == null || libro.portada == '') {
      libro.portada = '../../../../assets/dsBuffer.bmp.png';
    }
    this.loading = true;
    this._libroService
      .agregarLibro(libro)
      .then(() => {
        this.toastr.success(
          'El Libro se registro con exito',
          'Libro Registrado'
        );
        this.loading = false;
        this.router.navigate(['libros']);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  editarLibro(id: string) {
    this.loading = true;
    const libro: any = {
      nombre: this.createLibro.value.nombre,
      autor: this.createLibro.value.autor,
      descripcion: this.createLibro.value.descripcion,
      precio: this.createLibro.value.precio,
      categoria: this.createLibro.value.categoria,
      portada: this.createLibro.value.portada,
      fechaActualizacion: new Date(),
    };
    if (libro.portada == null || libro.portada == '') {
      libro.portada = '../../../../assets/dsBuffer.bmp.png';
    }
    this._libroService.ActualizarLibro(id, libro).then(() => {
      this.loading = false;
      this.toastr.info('Libro Actualizado correctamente', 'Libro Actualizado');
      this.router.navigate(['libros']);
    });
  }

  edLibro() {
    this.titulo = 'Agregar Libro';
    if (this.id != null) {
      this.titulo = 'Editar Libro';
      this.loading = true;
      this._libroService.EditarLibro(this.id).subscribe((data) => {
        this.loading = false;
        this.createLibro.setValue({
          nombre: data.payload.data()['nombre'],
          autor: data.payload.data()['autor'],
          descripcion: data.payload.data()['descripcion'],
          portada: data.payload.data()['portada'],
          precio: data.payload.data()['precio'],
          categoria: data.payload.data()['categoria'],
        });
      });
    }
  }
}
