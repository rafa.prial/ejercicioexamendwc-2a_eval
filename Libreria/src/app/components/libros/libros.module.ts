import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { ToastrModule } from 'ngx-toastr';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibrosRoutingModule } from './libros-routing.module';
import { ListLibrosComponent } from './list-libros/list-libros.component';
import { CreateLibrosComponent } from './create-libros/create-libros.component';


@NgModule({
  declarations: [
    ListLibrosComponent,
    CreateLibrosComponent,
  ],
  imports: [
    LibrosRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    MatIconModule,
    MatButtonModule,
    MatInputModule,
  ],
  providers:[]
})
export class LibrosModule { }
