import { CreateLibrosComponent } from './create-libros/create-libros.component';
import { ListLibrosComponent } from './list-libros/list-libros.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';


const routes : Routes = [
  { path: 'list-libros', component: ListLibrosComponent },
  { path: 'create-libros', component: CreateLibrosComponent },
  { path: 'editLibro/:id', component: CreateLibrosComponent },
  {path: '', component:ListLibrosComponent}
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class LibrosRoutingModule { }
