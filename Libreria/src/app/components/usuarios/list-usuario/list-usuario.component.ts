import { ToastrService } from 'ngx-toastr';
import { UsuarioService } from './../../../services/usuarios/usuario.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-usuario',
  templateUrl: './list-usuario.component.html',
  styleUrls: ['./list-usuario.component.css'],
})
export class ListUsuarioComponent implements OnInit {
  usuarios: any[] = [];
  constructor(
    private _usuarioService: UsuarioService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.getUsuarios();
  }
  getUsuarios() {
    this._usuarioService.getUsuarios().subscribe((data) => {
      this.usuarios = [];
      data.forEach((element: any) => {
        this.usuarios.push({
          id: element.payload.doc.id,
          ...element.payload.doc.data(),
        });
      });
    });
  }
  eliminarUsuario(id: string) {
    this._usuarioService
      .eliminarUsuario(id)
      .then(() => {
        this.toastr.error('Socio eliminado con exito', 'Socio Eliminado');
      })
      .catch((error) => {
        console.log(error);
      });
  }
}
