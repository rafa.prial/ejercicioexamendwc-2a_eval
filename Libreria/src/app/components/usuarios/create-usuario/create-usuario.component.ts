import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsuarioService } from './../../../services/usuarios/usuario.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-usuario',
  templateUrl: './create-usuario.component.html',
  styleUrls: ['./create-usuario.component.css'],
})
export class CreateUsuarioComponent implements OnInit {
  createUsuario: FormGroup;
  submitted = false;
  loading = false;
  id: string | null;
  titulo = 'Agregar Socio';
  constructor(
    private fb: FormBuilder,
    private _usuarioService: UsuarioService,
    private router: Router,
    private toastr: ToastrService,
    private aRoute: ActivatedRoute
  ) {
    this.createUsuario = this.fb.group({
      nombre: ['', Validators.required],
      correo: ['', Validators.required],
      edad: ['', Validators.required],
      dni: ['', Validators.required],
      numeroSocio: ['', Validators.required],
      foto: [''],
    });
    this.id = this.aRoute.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.edUsuario();
  }

  agregarEditarUsuario() {
    this.submitted = true;
    if (this.createUsuario.invalid) {
      return;
    }
    if (this.id == null) {
      this.agregarUsuario();
    } else {
      this.editarUsuario(this.id);
    }
  }

  agregarUsuario() {
    const usuario: any = {
      nombre: this.createUsuario.value.nombre,
      correo: this.createUsuario.value.correo,
      edad: this.createUsuario.value.edad,
      dni: this.createUsuario.value.dni,
      numeroSocio: this.createUsuario.value.numeroSocio,
      foto: this.createUsuario.value.foto,
      fechaActualizacion: new Date(),
      fechaSocio: new Date().toISOString().split('T')[0],
    };
    if (usuario.foto == null || usuario.foto == '') {
      usuario.foto =
        '../../../../assets/blank-profile-picture-gdffa26d0a_1280.png';
    }
    this.loading = true;
    this._usuarioService
      .agregarUsuario(usuario)
      .then(() => {
        this.toastr.success(
          'El Usuario se registro con exito',
          'Usuario Registrado'
        );
        this.loading = false;
        this.router.navigate(['/usuarios']);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  editarUsuario(id: string) {
    this.loading = true;
    const usuario: any = {
      nombre: this.createUsuario.value.nombre,
      correo: this.createUsuario.value.correo,
      edad: this.createUsuario.value.edad,
      dni: this.createUsuario.value.dni,
      numeroSocio: this.createUsuario.value.numeroSocio,
      foto: this.createUsuario.value.foto,
      fechaActualizacion: new Date(),
    };
    if (usuario.foto == null || usuario.foto == '') {
      usuario.foto =
        '../../../../assets/blank-profile-picture-gdffa26d0a_1280.png';
    }
    this._usuarioService.ActualizarUsuario(id, usuario).then(() => {
      this.loading = false;
      this.toastr.info(
        'Usuario Actualizado correctamente',
        'Usuario Actualizado'
      );
      this.router.navigate(['/usuarios']);
    });
  }

  edUsuario() {
    this.titulo = 'Agregar Socio';
    if (this.id != null) {
      this.titulo = 'Editar Socio';
      this.loading = true;
      this._usuarioService.EditarUsuario(this.id).subscribe((data) => {
        this.loading = false;
        this.createUsuario.setValue({
          nombre: data.payload.data()['nombre'],
          correo: data.payload.data()['correo'],
          numeroSocio: data.payload.data()['numeroSocio'],
          edad: data.payload.data()['edad'],
          dni: data.payload.data()['dni'],
          foto: data.payload.data()['foto'],
        });
      });
    }
  }
}
