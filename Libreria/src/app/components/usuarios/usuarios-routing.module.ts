import { ListUsuarioComponent } from './list-usuario/list-usuario.component';
import { CreateUsuarioComponent } from './create-usuario/create-usuario.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  { path: 'create-usuario', component: CreateUsuarioComponent },

  { path: 'editUsuario/:id', component: CreateUsuarioComponent },
  {path: '/usuarios', component:ListUsuarioComponent},
  {path: '', component:ListUsuarioComponent},
  // {path: '', component:ListUsuarioComponent}
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }
