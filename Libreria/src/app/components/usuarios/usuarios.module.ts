import { PrestamosRoutingModule } from './../prestamos/prestamos-routing.module';
import { CreateUsuarioComponent } from './create-usuario/create-usuario.component';
import { ListUsuarioComponent } from './list-usuario/list-usuario.component';
import { environment } from './../../../environments/environment';
import { ToastrModule } from 'ngx-toastr';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './../../app-routing.module';
// import { CreateLibrosComponent } from './create-libros/create-libros.component';
// import { ListLibrosComponent } from './list-libros/list-libros.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularFireModule } from '@angular/fire/compat';
import { BrowserModule } from '@angular/platform-browser';


@NgModule({
  declarations: [
    ListUsuarioComponent,
    CreateUsuarioComponent
  ],
  imports: [
    PrestamosRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    MatIconModule,
    MatButtonModule,
    MatInputModule,
  ]
})
export class UsuariosModule { }
