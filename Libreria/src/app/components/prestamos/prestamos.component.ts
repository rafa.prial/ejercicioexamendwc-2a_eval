import { PrestamoService } from './../../services/prestamos/prestamo.service';
import { LibroService } from 'src/app/services/libros/libro.service';
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-prestamos',
  templateUrl: './prestamos.component.html',
  styleUrls: ['./prestamos.component.css'],
})
export class PrestamosComponent implements OnInit {
  libros: any = [];
  prestamos: any = [];
  prestamo: any = [];
  prestamo2: any = [];
  usuarios: any[] = [];
  createLibro: FormGroup;
  submitted = false;
  loading = false;
  id: string | null;
  // prestamosList = this._prestamoService.ListAll();

  titulo = 'Prestamos';

  constructor(
    private fb: FormBuilder,
    private _libroService: LibroService,
    private _prestamoService: PrestamoService,
    private router: Router,
    private toastr: ToastrService,
    private aRoute: ActivatedRoute
  ) {
    this.createLibro = this.fb.group({
      nombre: [''],
      autor: ['', Validators.required],
      descripcion: ['', Validators.required],
      precio: ['', Validators.required],
      categoria: ['', Validators.required],
      portada: [''],
    });
    this.id = this.aRoute.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.getLibros();
    this.getPrestamos();
  }

  getLibros() {
    this._libroService.getLibros().subscribe((data) => {
      this.libros = [];
      data.forEach((element: any) => {
        this.libros.push({
          id: element.payload.doc.id,
          ...element.payload.doc.data(),
        });
      });
    });
  }
  getPrestamos() {
    this._prestamoService.ListAll().subscribe((data) => {
      this.prestamos = [];
      data.forEach((element: any) => {
        this.prestamos.push({
          id: element.payload.doc.id,
          ...element.payload.doc.data(),
        });
      });
    });
  }

  devolver(id: string) {
    const libro: any = {
      prestado: false,
      usuario: '',
      nombreUsuarioPrestado: '',
      fechaDevolucion: new Date(),

    };

    this.prestamo = this.prestamos.filter((x:any) => x.idLibro === id && x.devuelto === false);

    this.prestamo2 ={
      devuelto: true,
      fechaDevuelto: new Date(),
    }

    this._prestamoService.DevolverPrestamo(this.prestamo[0].id , this.prestamo2);

    //Cuando el libro es devuelto correctamente muestra un card con los siguientes datos:
    this._libroService.ActualizarLibro(id, libro).then(() => {
      this.toastr.info('Libro Devuelto correctamente', 'Libro Devuelto');
      this.router.navigate(['/home']);
    });
  }
}
