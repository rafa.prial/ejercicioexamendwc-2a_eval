import { PrestamosComponent } from './prestamos.component';
// import { PrestamosComponent } from './../prestamos.component';
// import { PedirComponent } from './../pedir/pedir.component';
// import { ListPrestamosComponent } from './../list-prestamos/list-prestamos.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListPrestamosComponent } from './list-prestamos/list-prestamos.component';
import { PedirComponent } from './pedir/pedir.component';
import { AuthGuard } from '../shared/guards/auth.guard';

const routes: Routes=[
  // { path: 'prestamos', component: PrestamosComponent },
  { path: 'pedirLibro/:id', component: PedirComponent },
  { path: 'list-prestamos', component: ListPrestamosComponent , canActivate:[AuthGuard]},
  {path: '', component:PrestamosComponent}
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PrestamosRoutingModule { }
