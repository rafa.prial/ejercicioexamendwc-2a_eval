import { NavbarComponent } from './../shared/navbar/navbar.component';
import { PrestamosRoutingModule } from './prestamos-routing.module';
import { PedirComponent } from './pedir/pedir.component';
import { PrestamosComponent } from './prestamos.component';
import { ListPrestamosComponent } from './list-prestamos/list-prestamos.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularFireModule } from '@angular/fire/compat';
import { environment } from './../../../environments/environment';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './../../app-routing.module';
import { BrowserModule } from '@angular/platform-browser';


@NgModule({
  declarations: [
    PrestamosComponent,
    ListPrestamosComponent,
    PedirComponent,
    // NavbarComponent
  ],
  imports: [
    PrestamosRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    MatIconModule,
    MatButtonModule,
    MatInputModule,
  ]
})
export class PrestamosModule { }
