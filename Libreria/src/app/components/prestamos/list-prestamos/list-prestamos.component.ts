import { PrestamoService } from './../../../services/prestamos/prestamo.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-prestamos',
  templateUrl: './list-prestamos.component.html',
  styleUrls: ['./list-prestamos.component.css']
})
export class ListPrestamosComponent implements OnInit {

  constructor(private _prestamoService: PrestamoService) { }
  prestamos: any[] = [];

  ngOnInit(): void {
    this.getPrestamos();

  }
  getPrestamos() {
    this._prestamoService.ListAll().subscribe((data) => {
      this.prestamos = [];
      data.forEach((element: any) => {
        this.prestamos.push({
          id: element.payload.doc.id,
          ...element.payload.doc.data(),
        });
      });
    });
  }

}
