import { PrestamoService } from './../../../services/prestamos/prestamo.service';
import { UsuarioService } from './../../../services/usuarios/usuario.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { LibroService } from 'src/app/services/libros/libro.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pedir',
  templateUrl: './pedir.component.html',
  styleUrls: ['./pedir.component.css'],
})
export class PedirComponent implements OnInit {
  libros: any = [];
  usuarios: any[] = [];
// prestamos: any[] = [];
  createLibro: FormGroup;
  createPrestamo: FormGroup;
  submitted = false;
  loading = false;
  id: string | null;
  titulo = 'Agregar Libro';

  constructor(
    private fb: FormBuilder,
    private _libroService: LibroService,
    private _prestamoService: PrestamoService,
    private aRoute: ActivatedRoute,
    private _usuarioService: UsuarioService,
    private router: Router,
    private toastr: ToastrService
  ) {
    this.createPrestamo = this.fb.group({
      nombreUser: ['', Validators.required],
    });

    this.createLibro = this.fb.group({
      nombre: [''],
      autor: [''],
      descripcion: [''],
      precio: [''],
      categoria: [''],
      portada: [''],
      fechaDevolucion: [''],
    });
    this.id = this.aRoute.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.getLibro();
    this.getUsuarios();
  }
  guardarPrestamo() {
    this.submitted = true;
    if (this.createLibro.invalid) {
      return;
    }
    if (this.id == null) {
      console.log('no llega id');
    } else {
      this.guardarPrestamos(this.id);
    }
  }

  getUsuarios() {
    this._usuarioService.getUsuarios().subscribe((data) => {
      this.usuarios = [];
      data.forEach((element: any) => {
        this.usuarios.push({
          id: element.payload.doc.id,
          ...element.payload.doc.data(),
        });
      });
    });
  }

  getLibro() {
    if (this.id != null) {
      this._libroService.EditarLibro(this.id).subscribe((data) => {
        this.loading = false;
        this.createLibro.setValue({
          nombre: data.payload.data()['nombre'],
          autor: data.payload.data()['autor'],
          descripcion: data.payload.data()['descripcion'],
          portada: data.payload.data()['portada'],
          precio: data.payload.data()['precio'],
          categoria: data.payload.data()['categoria'],
          fechaDevolucion: data.payload.data()['fechaDevolucion'],
        });
      });
    }
  }

  guardarPrestamos(id: string) {
    if (this.createPrestamo.value.nombreUser == '') {
      this.toastr.error('Usuario no seleccionado', 'Libro No Prestado');
      this.router.navigate(['/prestamos']);
    } else {
      const libro: any = {
        nombreUsuarioPrestado: this.createPrestamo.value.nombreUser,
        fechaPrestamo: new Date(),
        prestado: true,
      };
      const prestamo : any = {
        usuarioPrestado : this.createPrestamo.value.nombreUser,
        nombreLibro : this.createLibro.value.nombre,
        fechaPrestamo: new Date(),
        devuelto: false,
        fechaDevuelto: new Date(),
        idLibro: id,
      }
      this._prestamoService.agregarPrestamo(prestamo)
      this._libroService.ActualizarLibro(id, libro).then(() => {
        this.loading = false;
        this.toastr.info('Libro Prestado correctamente', 'Libro Prestado');
        this.router.navigate(['/prestamos']);
      });
    }
  }
  //Trabajo finalizado todo arreglado.
}
