import { Router } from '@angular/router';
import { AuthService } from './../../../../services/auth/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.css']
})
export class VerifyEmailComponent implements OnInit {

  constructor(public authService: AuthService, private router:Router) {}
  ngOnInit() {}
  userLogged = this.authService.getUserLogged();

  ngOnChange(){
    this.userLogged.subscribe(u => {if(u?.emailVerified == true){
      location.reload();
      this.router.navigate(['/home']);
    }});
  }
}
