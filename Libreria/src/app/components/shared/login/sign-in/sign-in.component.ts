import { AuthService } from './../../../../services/auth/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  usuario={
    email:'',
    password:''
  }
  register(){

    const {email,password}=this.usuario;
    this.authService.SignUp(email,password).then(res=>{
      console.log("se registro:", res);
    });

  }
  signIn(){
    // console.log(this.usuario)
    const {email,password}=this.usuario;
    this.authService.login(email,password).then(res=>{
      // console.log("se registro:", res);
    });

  }
  signInGoogle(){
    console.log(this.usuario)
    const {email,password}=this.usuario;
    this.authService.loginWithGoogle(email,password).then(res=>{
      console.log("se registro:", res);
    });

  }
  constructor(private authService: AuthService) { }

  ngOnInit(): void {

  }
logout(){
  this.authService.logout();
}
getUserLogged(){
  this.authService.getUserLogged().subscribe(res=>{
    console.log(res?.email);
  })
}

}
