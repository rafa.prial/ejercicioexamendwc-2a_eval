import { Router } from '@angular/router';
import { AuthService } from './../../../../services/auth/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {


  constructor(public authService: AuthService,public router: Router) { }

  ngOnInit(): void {
  }

  user = {
    email: '',
    password: ''
  }

  OnSignUp(){
    this.authService.SignUp(this.user.email, this.user.password).then(() => {
      this.router.navigate(['/verify'])
    });


  }
}
