import { SignUpComponent } from './components/shared/login/sign-up/sign-up.component';
import { VerifyEmailComponent } from './components/shared/login/verify-email/verify-email.component';
import { ForgotPassComponent } from './components/shared/login/forgot-pass/forgot-pass.component';
import { SignInComponent } from './components/shared/login/sign-in/sign-in.component';
import { PrestamosRoutingModule } from './components/prestamos/prestamos-routing.module';
import { ListPrestamosComponent } from './components/prestamos/list-prestamos/list-prestamos.component';
import { PedirComponent } from './components/prestamos/pedir/pedir.component';
import { PrestamosComponent } from './components/prestamos/prestamos.component';
import { ListUsuarioComponent } from './components/usuarios/list-usuario/list-usuario.component';
import { CreateUsuarioComponent } from './components/usuarios/create-usuario/create-usuario.component';
import { HomeComponent } from './components/shared/home/home.component';
import { CreateLibrosComponent } from './components/libros/create-libros/create-libros.component';
import { ListLibrosComponent } from './components/libros/list-libros/list-libros.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './components/shared/guards/auth.guard';
import { Error404Component } from './components/shared/error404/error404.component';

const routes: Routes = [
  { path: '', redirectTo: 'sign-in', pathMatch: 'full' }, //Direccion automatica al entrar
  { path: 'home', component: HomeComponent, canActivate:[AuthGuard]},
  { path: 'sign-in', component: SignInComponent },
  { path: 'verify', component: VerifyEmailComponent },
  { path: 'forgotpass', component: ForgotPassComponent },
  { path: 'sign-up', component: SignUpComponent },
  {path:'error', component:Error404Component},
  {path: 'libros', loadChildren:() => import('./components/libros/libros-routing.module').then(m => m.LibrosRoutingModule), canActivate:[AuthGuard]},
  {path: 'usuarios', loadChildren:() => import('./components/usuarios/usuarios-routing.module').then(m => m.UsuariosRoutingModule), canActivate:[AuthGuard]},
  {path: 'prestamos', loadChildren:() => import('./components/prestamos/prestamos-routing.module').then(m => m.PrestamosRoutingModule), canActivate:[AuthGuard]},
  {path: '**', redirectTo: 'error', pathMatch: 'full'}, //Con cualquier ruta que no este controlada envia a este
];

@NgModule({
  imports: [RouterModule.forRoot(routes),
 ],

  exports: [RouterModule],
})
export class AppRoutingModule {}
