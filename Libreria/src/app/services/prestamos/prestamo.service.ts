import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PrestamoService {

  constructor(private firestore: AngularFirestore) {}
  agregarPrestamo(prestamo: any):Promise<any>{
    return this.firestore.collection('prestamos').add(prestamo)




  }
  DevolverPrestamo(id: string, data: any):Promise<any>{
    return this.firestore.collection('prestamos').doc(id).update(data);
  }


  ListAll(): Observable<any> {
    return this.firestore
      .collection('prestamos')
      .snapshotChanges();
  }

}
