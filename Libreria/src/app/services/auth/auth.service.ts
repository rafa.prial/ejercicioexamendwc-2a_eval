import { VerifyEmailComponent } from './../../components/shared/login/verify-email/verify-email.component';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import firebase from 'firebase/compat/app';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userVerify?:any;
  constructor(private afauth: AngularFireAuth,  private router:Router) { }
  async SignIn(email:string, password:string){
    try {
      return await this.afauth.signInWithEmailAndPassword(email, password).then(() => {
        this.getUserLogged().subscribe(result =>{
          this.userVerify = (result !== null && result.emailVerified !== false) ? true:false;
          this.router.navigate(['home'])
        });
      });
    } catch (err) {
      console.log("Error: " + err);
      return null;
    }
  }

  async login(email:string,password:string){
    try{
      return await this.afauth.signInWithEmailAndPassword(email,password).then(() => {
        this.getUserLogged().subscribe(result =>{
          this.userVerify = (result !== null && result.emailVerified !== false) ? true:false;
          this.router.navigate(['home'])
        });
      });
    }catch(err){
      console.log("error login: ", err);
      return null;
    }
  }
  async loginWithGoogle(email:string,password:string){
    try{
      return await this.afauth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(() => {
        this.getUserLogged().subscribe(result =>{
          this.userVerify = (result !== null && result.emailVerified !== false) ? true:false;
          this.router.navigate(['home'])
        });
      });
    }catch(err){
      console.log("error login google: ", err);
      return null;
    }
  }

  async SignUp(email:string,password:string){
    try{
      return await this.afauth.createUserWithEmailAndPassword(email,password).then(() =>{
        this.SendVerificationMail();
        // this.router.navigate(['verify'])
      })

    }catch(err){
      console.log("error login: ", err);
      return null;
    }
  }

  get isLoggedIn(){
    try {
      this.getUserLogged().subscribe(result =>{
        this.userVerify = (result !== null && result.emailVerified !== false) ? true:false;
      });
    } catch (err) {
      console.log("Error: " + err);
      return false;
    }
    return this.userVerify;
  }
  getUserLogged(){
    return this.afauth.authState;
  }
  logout(){
    return this.afauth.signOut();
  }

  ForgotPassword(passwordResetEmail: string) {
    return this.afauth.sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        window.alert('Password reset email sent, check your inbox.');
      }).catch((err) => {
        window.alert(err)
      })
  }
  SendVerificationMail() {
    console.log(this.afauth.currentUser);
    return this.afauth.currentUser
      .then( u => u?.sendEmailVerification())
      .then(() => {
        this.router.navigate(['verify']);
      })
  }

}
