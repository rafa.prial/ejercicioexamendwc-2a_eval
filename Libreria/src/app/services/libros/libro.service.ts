import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LibroService {
  constructor(private firestore: AngularFirestore) {}
  agregarLibro(libro: any): Promise<any> {
    return this.firestore.collection('libros').add(libro);
  }

  getLibros(): Observable<any> {
    return this.firestore
      .collection('libros', (ref) => ref.orderBy('nombre', 'desc'))
      .snapshotChanges();
  }

  eliminarLibro(id: string): Promise<any> {
    return this.firestore.collection('libros').doc(id).delete();
  }
  EditarLibro(id: string): Observable<any> {
    return this.firestore.collection('libros').doc(id).snapshotChanges();
  }
  ActualizarLibro(id: string, data: any): Promise<any> {
    return this.firestore.collection('libros').doc(id).update(data);
  }
}
