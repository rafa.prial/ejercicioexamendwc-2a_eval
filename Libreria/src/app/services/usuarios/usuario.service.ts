import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UsuarioService {
  constructor(private firestore: AngularFirestore) {}

  agregarUsuario(usuario: any): Promise<any> {
    return this.firestore.collection('usuarios').add(usuario);
  }

  getUsuarios(): Observable<any> {
    return this.firestore
      .collection('usuarios', (ref) => ref.orderBy('nombre', 'desc'))
      .snapshotChanges();
  }

  eliminarUsuario(id: string): Promise<any> {
    return this.firestore.collection('usuarios').doc(id).delete();
  }
  EditarUsuario(id: string): Observable<any> {
    return this.firestore.collection('usuarios').doc(id).snapshotChanges();
  }
  ActualizarUsuario(id: string, data: any): Promise<any> {
    return this.firestore.collection('usuarios').doc(id).update(data);
  }
}
