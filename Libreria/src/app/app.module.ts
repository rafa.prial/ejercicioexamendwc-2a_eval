import { FormsModule } from '@angular/forms';
import { PrestamosModule } from './components/prestamos/prestamos.module';
import { LibrosModule } from './components/libros/libros.module';

import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { MatInputModule } from '@angular/material/input';
import { HomeComponent } from './components/shared/home/home.component';

import { environment } from '../environments/environment';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/compat/firestore';

import { AngularFireModule } from '@angular/fire/compat';
import { UsuariosModule } from './components/usuarios/usuarios.module';
import { SignInComponent } from './components/shared/login/sign-in/sign-in.component';
import { ForgotPassComponent } from './components/shared/login/forgot-pass/forgot-pass.component';
import { VerifyEmailComponent } from './components/shared/login/verify-email/verify-email.component';
import { SignUpComponent } from './components/shared/login/sign-up/sign-up.component';
import { Error404Component } from './components/shared/error404/error404.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    SignInComponent,
    ForgotPassComponent,
    VerifyEmailComponent,
    SignUpComponent,
    Error404Component,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    LibrosModule,
    PrestamosModule,
    UsuariosModule,
    FormsModule
  ],
  providers: [AngularFirestore],
  bootstrap: [AppComponent],
})
export class AppModule {}
